# Jobs

## ASP.NET Core MVC App

Time to find a new job!

### Database

Update Entity Framework:

```Shell
update-database
```

Create a new Migration

```Shell
add-migration JobsTable
```