﻿using System.Net;
using Jobs.Data;
using Jobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Jobs.Controllers
{
    [Route("api")]
    [ApiController]
    public class Posting : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        public Posting(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("jobs")]
        public IActionResult ReadAll()
        {
            var postings = _context.Jobs.ToArray();
            return Ok(postings);
        }

        [HttpGet("job")]
        public IActionResult ReadById(int id)
        {
            var posting = _context.Jobs.SingleOrDefault(x => x.Id == id);
            if (posting == null)
            {
                return NotFound();
            }
            return Ok(posting);
        }

        [HttpPost("job")]
        public IActionResult Create(Job posting)
        {
            if (posting.Id != 0)
            {
                return BadRequest();
            }
            _context.Jobs.Add(posting);
            _context.SaveChanges();
            return Ok();
        }
    }
}
