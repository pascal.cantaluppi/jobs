﻿namespace Jobs.Models;

public class Job
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public string? Location { get; set; }
    public string? Profession { get; set; }
    public int? Salary { get; set; }
    public DateTime? StartDate { get; set; }
    public string? CompanyMail { get; set; }
    public byte[]? CompanyLogo { get; set; }
    public string? ContactPhone { get; set; }
    public string? ContactMail { get; set; }
    public string? ContactWebsite { get; set; }
}